"use strict";

function createNewUser(firstName, lastName, date) {
  const [day, month, year] = date.split('.');
  const birthdayDate = new Date(`${year}-${month}-${day}`);

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getlogin: function () {
      return this.firstName.toLowerCase()[0] + this.lastName.toLowerCase();
    },
    birthday: birthdayDate,
    getAge: function () {
      const currentDate = new Date();
      if (this.birthday.getMonth() <= currentDate.getMonth() && this.birthday.getDate() <= currentDate.getDate()) {
        return currentDate.getFullYear() - this.birthday.getFullYear()
      } else {
        return (currentDate.getFullYear() - this.birthday.getFullYear()) - 1;
      }
    },
    getPassword: function () {
      return this.firstName.toUpperCase()[0] + this.lastName.toLowerCase() + this.birthday.getFullYear();
    }
  }

  return {
    login: newUser.getlogin(),
    age: newUser.getAge(),
    password: newUser.getPassword(),
  }
}

console.log(createNewUser(prompt("Enter the first name:"), prompt("Enter the last name:"), prompt("Enter the date of birthday")));